import random

number = random.randint(1,10)

number_of_guesses = 0

print("I'm thinking of a number from 1 to 10")

while number_of_guesses < 5:
    guess = int(input("please write your guess: "))
    number_of_guesses += 1
    if guess > number:
        print("your guess is too high")
    if guess < number:
        print("your guess is too low")
    if guess == number:
        break
if guess == number:
    print("good job! you guessed the number")
else:
    print("you lost! the number is: " + str(number))
