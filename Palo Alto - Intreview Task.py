import json

# consider input file with the following json structure:
json_data = {"process_name": "a.exe", "pid": 420, "parent_pid": 428},\
       {"process_name": "c.exe", "pid": 428, 'parent_pid': "null"},\
       {"process_name": "d.exe", "pid": 551, "parent_pid": 420},\
       {"process_name": "e.exe", "pid": 552, "parent_pid": 428},\
       {"process_name": "f.exe", "pid": 553, "parent_pid": "null"},\
       {"process_name": "g.exe", "pid": 4, "parent_pid": 553},\
       {"process_name": "b.exe", "pid": 7, "parent_pid": 4},\
       {"process_name": "h.exe", "pid": 11, "parent_pid": 7}

with open("json_write", "w", encoding='utf-8') as jsonFile:
    json.dump(json_data, jsonFile, ensure_ascii=False, indent=4, sort_keys=True)  # python 3
    jsonFile.close()

# output the process execution tree from json:
# Create a dict linking each pid to its parent
with open("json_write") as jsonData:
    jsonObjects = json.load(jsonData)
    ids = {}
    for d in jsonObjects:
        ppid, pid, name = d["parent_pid"], d["pid"], d["process_name"]
        ids.setdefault(ppid, {}).setdefault(pid, name)
    print(ids)

# Nest process name from each pid and it's parent dict
    def insert(lst, ppid, name):
        if ppid in ids:
            lst.append(name)
            for key, value in ids[ppid].items():
                print(value)
                insert(lst, key, value)
        else:
            lst.append(name)

    nested = []
    insert(nested, "null", " ")







