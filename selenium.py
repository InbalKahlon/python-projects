from selenium import webdriver
from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options
from selenium.webdriver.firefox.options import Options
import time
from pathlib import Path

# 1. Write a script which will open the following:
# a. Chrome – http://www.walla.co.il

def open_chrome():

    browser_chrome = webdriver.Chrome(executable_path='d:/')
    browser_chrome.get('http://www.walla.co.il')

# open_chrome()

# b. FireFox – http://www.ynet.co.il

def open_firefox():

    browser_firefox = webdriver.Firefox(executable_path='d:/geckodriver.exe')
    browser_firefox.get("http://www.ynet.co.il")

# open_firefox()

# 2. In one of the browsers you open do the following:
# a. Create a variable with the website’s title

def get_website_title(web_path):

    browser_chrome = webdriver.Chrome('d:/chromedriver.exe')
    browser_chrome.get(web_path)
    title = browser_chrome.title
    print(title)
    return title

# get_website_title('http:/...")

# b. Refresh website

def refresh_website():

    browser_chrome = webdriver.Chrome('d:/chromedriver.exe')
    browser_chrome.get('http://www.walla.co.il')
    time.sleep(3)
    browser_chrome.refresh()
    time.sleep(3)
    browser_chrome.close()

# refresh_website()

# c. Get website name and compare it to the variable you created in clause 1.
def compare_elements(web_path1, web_path2):

    title = get_website_title(web_path1)
    title2 = get_website_title(web_path2)

    if title == title2:
        print("titles are equal")
    else:
        print("titles are not equal")

# compare_elements("http:/www.walla.co.il", "http:/www.google.com")

# 3. Open a few browsers, locate any element, does the element has the same locators in the other browser?
def browsers_elements():
    # load chrome
    chrome_browser = webdriver.Chrome('d:/chromedriver.exe')
    chrome_browser.maximize_window()
    # open walla and search for element
    chrome_browser.get('http:/www.walla.co.il')
    walla_handle = chrome_browser.window_handles[0]
    walla_element = chrome_browser.find_element_by_link_text("חדשות").text
    print(walla_element)
    # open ynet and search for element
    chrome_browser.execute_script("window.open('http://www.ynet.co.il', 'new window')")
    ynet_handle = chrome_browser.window_handles[1]
    chrome_browser.switch_to.window(ynet_handle)
    time.sleep(7)
    ynet_element = chrome_browser.find_element_by_xpath("//*[text()='חדשות']").text
    print(ynet_element)
    # compare the 2 elements
    if ynet_element != walla_element:
        print("elements not equal")
    else:
        print("elements are equal")

# browsers_elements()

# 4. Create a test with the following:
# Open Google Translate web page
# Write anything in Hebrew in the text area
def translate():
    chrome_browser = webdriver.Chrome('d:/chromedriver.exe')
    chrome_browser.get('https://translate.google.com/')
    elem = chrome_browser.find_element_by_id("source")
    elem.send_keys('שלום' + Keys.RETURN)
    time.sleep(10)
    chrome_browser.close()

# translate()

# 5.
# Open Youtube web page
# Type a name of a song
# Click on search.
# edit: play the song (I had to)
def play_schwifty():
    chrome_browser = webdriver.Chrome('d:/chromedriver.exe')
    chrome_browser.get('https://www.youtube.com/')
    elem = chrome_browser.find_element_by_id("search")
    elem.send_keys('get schwifty' + Keys.RETURN)
    time.sleep(10)
    play = chrome_browser.find_element_by_link_text("Rick and Morty - Get Schwifty")
    play.click()
    time.sleep(80)

play_schwifty()

# 6.
# - Open Chrome browser on Google Translate website:
# https://translate.google.com/
# - Find translation text field (the one you send keys to)
# with 3 different locators and print the WebElement
# you created.
def translate_xpath():
    chrome_browser = webdriver.Chrome('d:/chromedriver.exe')
    chrome_browser.get('https://translate.google.com/')
    elem = chrome_browser.find_element_by_xpath("//input[@id='source', @class='orig tlid-source-text-input goog-textarea', @rows='1']")
    elem.send_keys('שלום' + Keys.RETURN)
    time.sleep(10)
    chrome_browser.close()
# translate()

# 7.
# - Open Chrome browser on Facebook website https://www.facebook.com/
# - Login into Facebook (No need to send me credentials).

# I don't have facebook so I used linkedin.
def login(email, password):
    browser = webdriver.Chrome(executable_path="d:/chromedriver.exe")
    browser.get("https://www.linkedin.com/login?fromSignIn=true&trk=guest_homepage-basic_nav-header-signin")
    time.sleep(10)
    username = browser.find_element_by_id("username").send_keys(email)
    pswd = browser.find_element_by_id("password").send_keys(password)
    button = browser.find_element_by_class_name("login__form_action_container")
    button.click()
    time.sleep(15)
# login(email, password)



#############
# CHALLENGES#
#############

# 8.
# - Open Chrome browser on any webpage.
# - Delete all cookies from browser.
# - Make sure all cookies are deleted by printing all cookies
# stored in the browser.

def delete_cookies():
    chrome_browser = webdriver.Chrome("d:/chromedriver.exe")
    chrome_browser.get('https://www.youtube.com/')
    print(chrome_browser.get_cookies())
    chrome_browser.delete_all_cookies()
    print(chrome_browser.get_cookies())
    time.sleep(10)
# delete_cookies()

# 9.
# - Open any browser on "Github" website.
# - https://github.com/
# - Enter “Selenium” keyword in search textfield
# - Press Enter to search (through code - of course).

def github_search():
    chrome_browser = webdriver.Chrome('d:/chromedriver.exe')
    chrome_browser.get('https://github.com/')
    elem = chrome_browser.find_element_by_name("q")
    elem.send_keys("selenium" + Keys.RETURN)
    time.sleep(10)
# github_search()

# 10.
# - Find a way to disable all extensions in
# o Chrome
# o Firefox
# o Internet Explorer.
# - Run browsers without extensions.


























