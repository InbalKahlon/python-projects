
# Python program to display the fibonacci sequence using recursion

# create fibonacci recursion function fn = f(n-1) + f(n-2)
def recur_fibo(n):
    if n <= 1:
        return n
    else:
        return (recur_fibo(n-1) + recur_fibo(n-2))

# create a var that represents a number range (here we see 1-10). It can be any range that you want.
# nterms = 10

# Option 2:

nterms = int(input("please type the number of the range of the fibonacci sequence: "))

# Put each number from range in the fibonacci recursion function and print the result
if nterms <= 0:
    print("please enter a positive number")
    # because we need a positive number for the function to work.
else:
    print("fibonacci sequence:")
    for i in range(nterms):
        print(recur_fibo(i))
